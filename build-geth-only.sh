#!/usr/bin/env bash
export PATH=$PATH:/usr/local/go/bin

build/env.sh go run build/ci.go install ./cmd/geth
chmod +x build/bin/geth
echo "Done building geth."

build/env.sh go run build/ci.go install ./cmd/bootnode
chmod +x build/bin/bootnode
echo "Done building bootnode."
